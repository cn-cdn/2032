import { basename as _basename, join } from 'path';
import { readdirSync, existsSync, unlinkSync } from 'fs';
import { program } from 'commander';
import { execSync } from 'child_process';

program.version('0.0.1');

program
    .requiredOption('-i, --input <input>', 'input directory')
    .requiredOption('-o, --output <output>', 'output directory');

program.parse(process.argv);

const options = program.opts();

for (const file of readdirSync(options.input)) {
    if (!file.match(/^\d\d-.+\.flac$/)) {
        continue;
    }
    const basename = _basename(file, '.flac');

    const input = join(options.input, file);
    const opusOutput = join(options.output, basename + '.opus');
    const aacOutput = join(options.output, basename + '.m4a');
    const metaOutput = join(options.output, basename + '.json');

    if (!existsSync(opusOutput)) {
        console.log(`Creating ${opusOutput}`);

        // 160kbps
        // with https://wiki.hydrogenaud.io/index.php?title=Opus
        const cmd = `ffmpeg -i ${input} -vn -c:a libopus -b:a 160K ${opusOutput}`
        console.log(cmd);
        execSync(cmd);
    }

    if (!existsSync(aacOutput)) {
        console.log(`Creating ${aacOutput}`);

        const dumpMetaCmd = `ffprobe -v 0 -of json -show_format ${input} > ${metaOutput}`;
        console.log(dumpMetaCmd);
        execSync(dumpMetaCmd);

        // profile: AAC-LC
        // bitrate-mode: VBR, ~64+64 kbps
        // with https://wiki.hydrogenaud.io/index.php?title=Fraunhofer_FDK_AAC
        const cmd = `flac -s -d -c ${input} | fdkaac --ignorelength --moov-before-mdat --profile 2 --bitrate-mode 4 -o ${aacOutput} --tag-from-json=${metaOutput}?format.tags -`
        console.log(cmd);
        execSync(cmd);

        unlinkSync(metaOutput);
    }
};
