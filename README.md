# Binary files of Complex Numbers & Victor Argonov Project

# Encoding settings
As defined in [`build.js`](build.js), script generates AAC and Opus versions from Flac files. AAC files are needed mostly [to support playback in Safari](https://caniuse.com/opus) (including iPhones). At this moment bitrate is set to 160kbps for Opus (marked as transparent in [Hydrogenaudio Knowledgebase](https://wiki.hydrogenaud.io/index.php?title=Opus#Music_encoding_quality)) and VBR profile 4 (~128kbps)](https://wiki.hydrogenaud.io/index.php?title=Fraunhofer_FDK_AAC) for AAC.

# Rebuilding or adding new files
1. Install ffmpeg, fdkaac and opus (e. g. run `sudo apt-get install ffmpeg fdkaac opus`)
2. Download files from https://drive.google.com/drive/folders/1IfQERQ2hdd7sCvmnPAbqx3URyZ6WRHEe
3. Delete opus and m4a files from `public` directory (if you want to regenerate them)
4. Run:
```
git clone https://gitlab.com/cn-cdn/2032.git
npm i
npm run build <path_to_your_downloads_directory>/2007-2032-legend-of-a-lost-future
```

# Deploy to Heroku
1. Configuring the application:
```
npm i
git remote add heroku https://git.heroku.com/cn-cdn-2032.git
npm run heroku login
npm run heroku config:set HEROKU_BUILDPACK_GIT_LFS_REPO=https://gitlab.com/cn-cdn/2032.git
npm run heroku -- buildpacks:add --index 1 https://github.com/raxod502/heroku-buildpack-git-lfs.git
npm run heroku -- buildpacks:add --index 2 heroku-community/static
```
2.  Run:
```
npm run heroku-deploy
```

## License
### 2032: Legend of a Lost Future (c) by Victor Argonov Project

2032: Legend of a Lost Future is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
